import setuptools

setuptools.setup(
  name = 'pyuo',         # How you named your package folder (MyLib)
  version = '0.0.2',      # Start with a small number and increase it with every change you make
  license='GPL',        # Chose a license from here: https://help.github.com/articles/licensing-a-repository
  description = 'ultima online client',   # Give a short description about your library
  author = 'Gabriele Tozzi',                   # Type in your name
  author_email = 'gabriele@tozzi.eu',      # Type in your E-Mail
  packages=setuptools.find_packages(),
  classifiers=[
    'Intended Audience :: Developers',      # Define that your audience are developers
    'Topic :: Software Development :: Build Tools',
    'License :: OSI Approved :: GPL',   # Again, pick a license
    'Programming Language :: Python :: 3',    #Specify which pyhton versions that you want to support
  ],
  python_requires='>=3.4',
)